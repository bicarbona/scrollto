<?php
/**
 * Plugin Name: Headway Scroll To
 * Plugin URI:  http://wordpress.org/plugins
 * Description: Scroll to / Scroll Nav / Visual Nav
 * Version:     0.1.0
 * Author:      bicarbona
 * Author URI:  
 * License:     GPLv2+
 * Text Domain: scroll_to
 * Domain Path: /languages
 */


define('SCROLL_TO_BLOCK_VERSION', '0.1.0');

/**
 * Everything is ran at the after_setup_theme action to insure that all of Headway's classes and functions are loaded.
 **/
add_action('after_setup_theme', 'scroll_to_register');
function scroll_to_register() {

	if ( !class_exists('Headway') )
		return;

	require_once 'includes/Block.php';
	require_once 'includes/BlockOptions.php';
	//require_once 'design-editor-settings.php';

	return headway_register_block('scroll_toBlock', plugins_url(false, __FILE__));
}

/**
 * Prevent 404ing from breaking Infinite Scrolling
 **/
add_action('status_header', 'scroll_to_prevent_404');
function scroll_to_prevent_404($status) {

	if ( strpos($status, '404') && get_query_var('paged') && headway_get('pb') )
		return 'HTTP/1.1 200 OK';

	return $status;
}

/**
 * Prevent WordPress redirect from messing up featured board pagination
 */
add_filter('redirect_canonical', 'scroll_to_redirect');
function scroll_to_redirect($redirect_url) {

	if ( headway_get('pb') )
		return false;

	return $redirect_url;
}