<?php

class scroll_toBlock extends HeadwayBlockAPI {

    public $id = 'scroll_to';
    public $name = 'Headway Scroll To';
    public $options_class = 'scroll_toBlockOptions';
    public $description = 'Scroll to / Scroll Nav / Visual Nav';

    
	function enqueue_action($block_id) {

		/* CSS */
		wp_enqueue_style('headway-pin-board', plugin_dir_url(__FILE__) . '/css/scroll-to.css');		

		/* JS */
		wp_enqueue_script('headway-scroll-to', plugin_dir_url(__FILE__) . '/js/jquery.visualNav.min.js', array('jquery'));		

	}
	
	// public static function init_action($block_id, $block) 
    // {

    // }


    // public static function enqueue_action($block_id, $block, $original_block = null)
    // {

    // }


    // public static function dynamic_css($block_id, $block, $original_block = null)
    // {

    // }


	function dynamic_js($block_id, $block = false) {
	
		if ( !$block )
			$block = HeadwayBlocksData::get_block($block_id);
	
		$js = "
		jQuery(document).ready(function() {
			
		jQuery('#scroll-nav').visualNav({
			// content class to get height of the section.
			contentClass      : 'section',
			// css class applied to menu when a link is selected (highlighted).
			selectedClass     : 'current',
			// don't stop animation on mousewheel
			stopOnInteraction : false
		});
		});
		";
	
		return $js;
	
	}

    public function setup_elements() {
        $this->register_block_element(array(
            'id' => 'scroll-current',
            'name' => 'Current',
            'selector' => '.block-type-scroll_to .current',
           // 'properties' => array('property1', 'property2', 'property3'),
            'states' => array(
                'Hover' => '.block-type-scroll_to .current:hover',
                )
            ));
    }

	function article_title($block) {
		$html_tag = parent::get_setting($block, 'title-html-tag', 'h1');
		$linked = parent::get_setting($block,'title-link', true);
		$shorten = parent::get_setting($block,'title-shorten', true);

		/* Shorten Title */
		$title_text = get_the_title($id);
		$title_length = mb_strlen($title_text);
		$limit = parent::get_setting($block,'title-limit', 20);
		$title = substr($title_text, 0, $limit);
		if ($title_length > $limit) 
			$title .= "...";

		if (!$shorten)
			$title = get_the_title($id);

		if($linked)
			return '<' . $html_tag . ' class="entry-title">
			<a href="'. get_post_permalink($id) .'" rel="bookmark" title="'. the_title_attribute (array('echo' => 0) ) .'">'. $title .'</a>
		</' . $html_tag . '>';
		
		return '<' . $html_tag . ' class="entry-title">
			'. $title .'
		</' . $html_tag . '>';

		return $html_tag;
	}


    public function content($block) {
        /* CODE HERE */
		$repeater_posts = parent::get_setting($block, 'repeater-post' , array());



$post_ids = array();

foreach ( $repeater_posts as $repeater_post ) {


		 $post_ids[] = headway_fix_data_type(headway_get('post-id', $repeater_post));;
}



$post_type = parent::get_setting($block, 'post-type', false);

 $query_args = array( 
	'post__in' => $post_ids,
	'post_type' => $post_type
	);

// Debug
//print_r($query_args);
$query_args['post_type'] = parent::get_setting($block, 'post-type', false);


 $query = new WP_Query( $query_args );


	while ( $query->have_posts() ) {$query->the_post();

/**
title
**/
		// $linked = parent::get_setting($block,'title-link', true);
		// $shorten = parent::get_setting($block,'title-shorten', true);

		// /* Shorten Title */
		// $title_text = get_the_title($id);
		// $title_length = mb_strlen($title_text);
		// $limit = parent::get_setting($block,'title-limit', 20);
		// $title = substr($title_text, 0, $limit);
		// if ($title_length > $limit) 
		// 	$title .= "...";

		// if (!$shorten)
		// 	$title = get_the_title($id);


		// if(!$linked)
		// 	$main_title = '<' . $html_tag . ' class="entry-title">'. get_the_title() .'</' . $html_tag . '>';

		// if($linked)
		// 	$main_title = '<' . $html_tag . ' class="entry-title">
		// 	<a href="'. get_post_permalink($id) .'" rel="bookmark" title="'. the_title_attribute (array('echo' => 0) ) .'">'. $title .'</a>
		// </' . $html_tag . '>';

		// echo $main_title;
		 the_content();
		// if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
  // 			the_post_thumbnail();
		// }
		// edit_post_link();

    

echo ' <br />';
}



	//echo	$post_id = headway_get('post-id', $repeater_post);

		
    }

    
}